﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {

    [SerializeField] int hitPoints = 10;
    [SerializeField] ParticleSystem hitParticlePrefab;
    [SerializeField] ParticleSystem destroyParticlePrefab;
    [SerializeField] AudioClip enemyHitSFX;
    [SerializeField] AudioClip enemyDeathSFX;

    AudioSource myAudioSrouce;


    // Use this for initialization
    void Start () {
        myAudioSrouce = GetComponent<AudioSource>();
	}
	


    private void OnParticleCollision(GameObject other)
    {
      
        ProcessHit();
        if (hitPoints <= 0)
        {
            KillEnemy();
        }
    }

    private void ProcessHit()
    {
        hitPoints = hitPoints - 1;
        hitParticlePrefab.Play();
        myAudioSrouce.PlayOneShot(enemyHitSFX);
    }

    private void KillEnemy()
    {
        
        var vfx = Instantiate(destroyParticlePrefab, transform.position, Quaternion.identity);
        
        vfx.Play();
        float destroyDelay= vfx.main.duration;
        Destroy(vfx.gameObject,destroyDelay);
       
        AudioSource.PlayClipAtPoint(enemyDeathSFX, Camera.main.transform.position);
        

        Destroy(gameObject);

    }

}
