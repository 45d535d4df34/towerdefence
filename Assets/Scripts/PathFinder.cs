﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour {

    [SerializeField] WayPoint startWaypoint,endWayPoint;

    Dictionary<Vector2Int, WayPoint> grid  = new Dictionary<Vector2Int, WayPoint>();
    Queue<WayPoint> queue = new Queue<WayPoint>();
    bool isRunning = true; 
    WayPoint searchCenter;
    private List <WayPoint> path = new List<WayPoint>(); 

    Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    public List<WayPoint> GetPath()
    {
        if (path.Count == 0) 
        {
            CalculatePath();
        }
        return path;
    }

    private void CalculatePath()
    {
        LoadBlocks();
        //ColorStartAndEnd();
        BreadthFirstSearch();
        CreatePath();
    }

    private void CreatePath()
    {
        SetAsPath(endWayPoint);
        path.Add(endWayPoint);
        endWayPoint.isPlaceable = false;

        WayPoint previous = endWayPoint.exploredFrom;
        while(previous != startWaypoint)
        {
            SetAsPath(previous);
            previous = previous.exploredFrom;
           
        }
        SetAsPath(startWaypoint);
        path.Reverse();
    }

    private void SetAsPath(WayPoint waypoint)
    {
        path.Add(waypoint);
        waypoint.isPlaceable = false;
    }

    private void BreadthFirstSearch()
    {
        queue.Enqueue(startWaypoint);

        while (queue.Count > 0 && isRunning)
        {
            searchCenter = queue.Dequeue();
            searchCenter.isExplored = true;
            HaltIfEndFound();
            ExploreNeigbours();
        }
    }

    private void HaltIfEndFound()
    {
        if(searchCenter == endWayPoint)
        {
            isRunning = false;
        }
    }

    private void ExploreNeigbours()
    {
        if (!isRunning){return;}

        foreach(Vector2Int direction in directions)
        {
            Vector2Int neigborCoordinates = searchCenter.GetGridPos() + direction;
            if(grid.ContainsKey(neigborCoordinates))
            {
                QueueNewNeigbors(neigborCoordinates);
            }
        }
    }

    private void QueueNewNeigbors(Vector2Int neigborCoordinates)
    {
        WayPoint neighbor = grid[neigborCoordinates];
        if (neighbor.isExplored || queue.Contains(neighbor))
        {

        }
        else
        {
            queue.Enqueue(neighbor);
            neighbor.exploredFrom = searchCenter;
        }  
    }

    private void LoadBlocks()
    {
        var waypoints = FindObjectsOfType<WayPoint>();
        foreach(WayPoint waypoint in waypoints)
        {
            var gridPos = waypoint.GetGridPos();
            if (grid.ContainsKey(gridPos))
            {

            }
            else
            {
                grid.Add(gridPos, waypoint);
            }
        }
    }

}
