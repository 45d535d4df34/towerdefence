﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{

    [SerializeField] int towerLimit = 5;
    [SerializeField] Tower towerPrefab;
    [SerializeField] Transform towerParentTransform;


    Queue<Tower> towerQueue = new Queue<Tower>();

    public void AddTower(WayPoint baseWaypoint)
    {
        print(towerQueue.Count);
        int numTowers = towerQueue.Count;

        if (numTowers < towerLimit)
        {
            InstantiateNewTower(baseWaypoint);
        }
        else
        {
            MoveExistingTower(baseWaypoint);
        }
    }

    private void InstantiateNewTower(WayPoint baseWaypoint)
    {
        var newTower = Instantiate(towerPrefab, baseWaypoint.transform.position, Quaternion.identity);
        newTower.transform.parent = towerParentTransform;
        baseWaypoint.isPlaceable = false;
        //set the base waypoins
        newTower.baseWaypoint = baseWaypoint;
        baseWaypoint.isPlaceable = false;
        towerQueue.Enqueue(newTower);
    }

    private void MoveExistingTower(WayPoint newBaseWayPoint)
    {
        var oldTower = towerQueue.Dequeue();

        // set the placable flags
        oldTower.baseWaypoint.isPlaceable = true;
        newBaseWayPoint.isPlaceable = false;

        oldTower.baseWaypoint = newBaseWayPoint;

        oldTower.transform.position = newBaseWayPoint.transform.position;
        //set the basewaypoints

        //Put the old tower on the top of the queue
        towerQueue.Enqueue(oldTower);

    }


}