﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] int enemyStartHealth = 2;

    private int currentEnemyHealth;
	// Use this for initialization

	void Start () {
        currentEnemyHealth = enemyStartHealth;

    }
	
	// Update is called once per frame
	void Update () {
        CheckIfEnemyDead();
	}

    public void DecreaseEnemyHealth()
    {
        currentEnemyHealth -= 1;
        print("Called");
    }

    public void CheckIfEnemyDead()
    {
        if (currentEnemyHealth <= 0)
        {
            Destroy(gameObject);
            
        }
    }
}
