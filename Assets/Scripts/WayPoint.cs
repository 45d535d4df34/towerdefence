﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour {

    //Public Ok as it is a data class
    public bool isExplored = false; 
    public WayPoint exploredFrom;
    public bool isPlaceable = true;
    //[SerializeField] Tower towerPrefab;

    Vector2Int gridPos;

    const int gridSize = 10;


    public int GetGridSize()
    {
        return gridSize;
    }


    public Vector2Int GetGridPos()
    {
        return new Vector2Int(
        Mathf.RoundToInt(transform.position.x / gridSize)  ,
        Mathf.RoundToInt(transform.position.z / gridSize) 
        );
    }

    public void SetTopColor(Color color)
    {
        MeshRenderer topMeshRender = transform.Find("Top").GetComponent<MeshRenderer>();
        topMeshRender.material.color = color;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && isPlaceable)
        {
            //Instantiate(towerPrefab, transform.position, Quaternion.identity);
            //isPlaceable = false;
            FindObjectOfType<TowerFactory>().AddTower(this);
        }
        else
        {
            print("Cant Place here");
        }
        
    }

}
