﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

    [Range(0.1f, 120f)]
    [SerializeField] float secondsBetweenSpawns = 2f;
    [SerializeField] EnemyMovement enemyPrefab;
    [SerializeField] Transform enemyParentTransform;
    [SerializeField] Text spawnedEnemiesText;
    [SerializeField] AudioClip spawnedEnemySFX;

    private int spawnedEnemies = 0;


    private int x = 0;
    public int numberOfEnemies=5;

    // Use this for initialization
    void Start () {
        StartCoroutine(SpawnEnemy());
        spawnedEnemiesText.text = spawnedEnemies.ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    
    IEnumerator SpawnEnemy()
    {
        //while (true)
        //{
            AddScore();
            GetComponent<AudioSource>().PlayOneShot(spawnedEnemySFX);
            var newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            newEnemy.transform.parent = enemyParentTransform;
            x = x + 1;
            yield return new WaitForSeconds(secondsBetweenSpawns);
        //}
        //Instantiate(enemyPrefab,GameObject.transform.positio 
    }

    private void AddScore()
    {
        spawnedEnemies = spawnedEnemies + 1;
        spawnedEnemiesText.text = spawnedEnemies.ToString();
    }
}

